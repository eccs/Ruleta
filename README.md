# Ruleta
On-stage software for Ruleta: Dance, music and interactive digital media improvisation session.

*Ruleta* (Roulette) is an event of performing instant creation based on chance. Random operations determine the course of action for dancers and musicians immerse in interactive digital environments. Roulette’s purpose is to open a communication space around improvisation and the use of technology in the stage.

This Processing sketch is the program that is executed and projected during the shows, randomly determining the setup of each scene.

## Media
* [*Ruleta* in escenaconsejo.org](http://escenaconsejo.org/trayectoria/coreografias/ruleta/)

## Requirements
* [Processing 3.0](http://processing.org)

## How to use
* The lists of options to be chosen can be found in the data folder as separate text files.
* Spacebar is the only required live input: it fades in the screen, rolls the roulette, and starts the scene timer.
* The software will automatically decide when the final scene has to appear, based on the `TIMERTOTALTIME` and `REMAININGTIMETOEND` constants. At the end of the final scene it will automatically fade out.
* A second session will start from zero if after the fade out the spacebar is pressed again.
