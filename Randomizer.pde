/*
    Ruleta
    On-stage software for Ruleta: Dance, music and interactive digital media improvisation session.

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Randomizer{
	int nMin; // Minimum number of options that will be returned
	int nMax; // Maximum number of options that will be returned
	int N; // Total of options
	String filename; // Name of the plain text file (list of strings) with the options
	String title;

	String[] strings; // Strings retrieved from the plain text file
	boolean[] taken; // Flags: has the string been selected?

	ArrayList<String> list; // List that will be returned

	// Constructor
	Randomizer(String ti,String fn, int min, int max){
		title = ti;
		filename = fn;
		nMin = min;
		nMax = max;
		list = new ArrayList<String>();

		loadFile();
	}

	Randomizer(String ti, String fn, int max){
		this(ti,fn,1,max);
	}

	// Public
	public void genNewList(){
		int size = int(random(nMin,nMax+1)); // Select the random size of the list
		int index;
		reset();

		while(list.size()<size){
			do{ // Select a random unselected index
				index = int(random(N)); // Select a random index
			}while(taken[index]);

			taken[index] = true; // The index is now taken

			list.add(strings[index]);
		}
	}

	public void reset(){
		list.clear();
		clearTaken();
	}

	public ArrayList<String> getList(){
		return list;
	}

	public int getListSize(){
		return list.size();
	}

	public String getTitle(){
		return title;
	}

	public void printList(){
		println("---");
		println(filename);
		println(list.size());
		for(int i=0;i<list.size();i++){
			println(list.get(i));
		}
		println("---");
	}

	public void updateFile(){
		loadFile();
	}

	public void setMinMax(int nMinMax){
		nMin = nMinMax;
		nMax = nMinMax;
		loadFile();
	}

	public void setMinAndMax(int min, int max){
		nMin = min;
		nMax = max;
		loadFile();
	}


	// Private
	private void loadFile(){
		strings = loadStrings(filename);
		N = strings.length;
		taken = new boolean[N];
		clearTaken();
	}

	private void clearTaken(){
		for(int i=0;i<N;i++){
			taken[i] = false;
		}
	}



}
