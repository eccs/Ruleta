/*
    Ruleta
    On-stage software for Ruleta: Dance, music and interactive digital media improvisation session.

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

Randomizer[] ran;
int nrandomizer; // Number of randomizers

PGraphics pg;

PFont font;
PFont fontbig;
PFont fontres;
PFont fonttime;
String text,c;
int size; // textsize
int fontsize_title;
int fontsize_results;

int nescena;


boolean playing;


static final int RESET = -1;
static final int FADEIN = -2;
static final int FADEOUT = -3;
static final int IDLE = 0; 
static final int RUN = 1;
static final int RESULTS = 2;
static final int T1  = 3;
static final int TIMERIDLE = 4;
static final int TIMERRUN = 5;
static final int TIMERFINISHED = 6;
static final int T2 = 7;
int estado;

// User-configurable constants
static final int NBAILARINES = 11;
static final int NMUSICOS = 8;

static final int NBAILARINESMIN = 2;
static final int NMUSICOSMIN = 2;
static final int NBAILARINESMAX = 7;
static final int NMUSICOSMAX = 4;

static final int REMAININGTIMETOEND = 13; // Minutes
static final int TIMERTOTALTIME = 60; // minutes


int timerRuleta;
int timerTInicial;

Timer timer;

Timer timertotal;
boolean escenaFinal;

float alphaBg;
float alphaTxt;
float alphaLetras;

boolean flashTransition;

boolean logNewResult;

void setup(){
//	size(1200,800,P3D);
	fullScreen(P3D);
	pg = createGraphics(width,height,P3D);

	font = loadFont("font/JockeyOne-Regular-48.vlw");
	fontbig = loadFont("font/JockeyOne-Regular-80.vlw");
	fontres = loadFont("font/KontrapunktBold-48.vlw");
	fonttime = loadFont("font/KontrapunktBold-80.vlw");

	text = "Evento de creación escénica instantánea basada en el azar";
	size=20;
	fontsize_title = 48;
	fontsize_results = 38;

	timerTInicial = 100;



	nrandomizer = 6;
	ran = new Randomizer[nrandomizer];


	// Randomizer args: Name, textfile, minimum options to retrieve, maximum options to retrieve
	ran[0] = new Randomizer("DANZA","coreografia.txt",1,3);
	ran[1] = new Randomizer("MÚSICA","musica.txt",1,3);
	ran[2] = new Randomizer("ARTE DIGITAL","visuales.txt",1,3);
	ran[3] = new Randomizer("BAILARINES","bailarines.txt",NBAILARINESMIN,NBAILARINESMAX);
	ran[4] = new Randomizer("MÚSICOS","musicos.txt",NMUSICOSMIN,NMUSICOSMAX);
	ran[5] = new Randomizer("DURACIÓN","duraciones.txt",1);





	timertotal = new Timer();
	timer = new Timer();

	setupReset();

	flashTransition = false;
}

void setupReset(){
	logNewResult = false;
	estado = RESET;
	nescena = 1;
	resetTimerTotal();
	escenaFinal = false;
	alphaBg = 255;

	ran[3].setMinAndMax(NBAILARINESMIN,NBAILARINESMAX);
	ran[4].setMinAndMax(NMUSICOSMIN,NMUSICOSMAX);

	resetAll();
}

void draw(){
	drawBackground();
	drawTitles();

	timertotal.update();

	if(estado==RESET || estado==FADEIN || estado==FADEOUT){
		fill(0,alphaBg);
		rect(0,0,width,height);
	}

	if(estado==RUN || estado==TIMERRUN){
		fill(255,alphaBg);
		rect(0,0,width,height);
		if(alphaBg>0){
			alphaBg -=2;
		}
		else{
			alphaBg = 0;
		}
	}

	switch(estado){
		case RESET:
		break;

		case FADEIN:
			if(timerRuleta>0){
				timerRuleta--;
				alphaBg = map(timerRuleta,0,timerTInicial,0,255);
			}
			else{
				alphaBg = 0;
				estado = IDLE;
			}
		break;

		case FADEOUT:
			if(timerRuleta>0){
				timerRuleta--;
				alphaBg = map(timerTInicial-timerRuleta,0,timerTInicial,0,255);
			}
			else{
				setupReset();
			}
		break;

		case IDLE:
		break;

		case RUN:
			genOne();
			if(timerRuleta>0){
				timerRuleta--;
			}
			else{
				newResults();
			}
		break;

		case RESULTS:
			if(logNewResult){
				logFrame();
				logNewResult = false;
			}
			estado = T1;
			timerRuleta = timerTInicial;

		break;

		case T1:
			if(timerRuleta>0){
				timerRuleta--;
			}
			else{
				estado = TIMERIDLE;
			}

		break;

		case TIMERRUN:
			if(timer.isRunning()){
				timer.update();
			}
			else{
				estado = TIMERFINISHED;
				timerRuleta = 10;

				resetAll();
			}

		break;

		case TIMERFINISHED:
			if(timerRuleta==0){
					if(!escenaFinal){ // If not in last scene
						gotoIdle();
					}
					else{
						estado = FADEOUT;
						timerRuleta = timerTInicial;
					}
			}

		break;

		case T2:
			if(timerRuleta>0){
				timerRuleta--;
			}
			else{
				estado = IDLE;

			}

		break;

	}

	/*
	textSize(20);
	fill(255,0,0);
	text(frameRate,20,20);
	*/

	randomSeed(millis());


}

void keyPressed(){
	switch(key){
		case ' ':
			switch(estado){
				case RESET:
					estado = FADEIN;
					timerRuleta = timerTInicial;

					timertotal.start(); // Start total timer
				break;
				case IDLE:
					estado = RUN;
//					timerRuleta = int(random(300,600));
					timerRuleta = 22*60;
					alphaBg = 255;
				break;
				case RESULTS:
					estado = T1;
					timerRuleta = timerTInicial;
				break;
				case TIMERIDLE:
					println("Starting timer...");
					estado = TIMERRUN;
					alphaBg = 255;
					timer.start();

				break;

				case TIMERFINISHED:
					if(!escenaFinal){ // If not in last scene
						gotoIdle();
					}
					else{
						estado = FADEOUT;
						timerRuleta = timerTInicial;
					}
				break;



			}
		break;


		case 's': // Save screenshot
			saveFrame();
		break;

		case 'R': // Reload
			for(int i=0;i<ran.length;i++){
				ran[i].updateFile();
			}
		break;

		case 'r': // Reset nescena
			nescena = 1;
		break;

		case '0':
			resetTimerTotal();
			estado = RESET;
			nescena = 1;
			alphaBg = 255;

		break;

		case CODED:
			switch(keyCode){
				case RIGHT:
					newResults();

				break;
				case DOWN:
					gotoIdle();
//					estado = IDLE;
				break;

			}
		break;
	}
}

void gotoIdle(){
	estado = T2;
	timerRuleta = timerTInicial;
	nescena++;
	resetAll();

	if(isTimeForEscenaFinal()){
		escenaFinal = true;
		ran[3].setMinMax(NBAILARINES);
		ran[4].setMinMax(NMUSICOS);
//		ran[3] = new Randomizer("BAILARINES","bailarines.txt",NBAILARINES,NBAILARINES);
//		ran[4] = new Randomizer("MÚSICOS","musicos.txt",NMUSICOS,NMUSICOS);
	}
}

void newResults(){
	genAll();
	estado = RESULTS;
	logNewResult = true;

}

void resetAll(){
	for(Randomizer r : ran){
		r.reset();	
	}
}

void genOne(){
	int i = int(random(nrandomizer));
	ran[i].genNewList();

}

void genAll(){
	for(Randomizer r : ran){
		r.genNewList();	
	}

}

void logFrame(){
	save(String.format("screen-%04d%02d%02d%02d%02d%02d.png",year(),month(),day(),hour(),minute(),second()));
}


void drawTitles(){
	String t;
	float tw;
	fill(255,200);

	switch(estado){
		case T1:
			alphaTxt = map(timerRuleta,0,timerTInicial,0,1.0);
		break;
		case T2:
			alphaTxt = map(timerTInicial-timerRuleta,0,timerTInicial,0,1.0);
		break;
		default:
			alphaTxt = 1.0;
		break;

	}

	// Draw Title
//	if(estado!=TIMERIDLE && estado!=TIMERRUN && estado!=TIMERFINISHED){
		textFont(fontbig,80);
		if(estado == RUN){
			fill(255,random(100,200));
		}
		else{
			fill(255,200);
		}
		textSize(80);
		t = "RULETA";
		tw = textWidth(t);
		text(t,(width/3-tw)/2,80);
//	}

	if(estado == RUN){
		fill(255,random(100,200));
	}
	else{
		fill(255,200);
	}

	textFont(fontbig,80);
	textSize(60);
	if(escenaFinal){
		text("Escena FINAL",width*3/4,70);
	}
	else{
		text("Escena #"+nescena,width*3/4,70);
	}


	int dx = width/(nrandomizer/2);
	int y0 = 160;
	int dy = (height-y0)/3;
	int x,y;

	textFont(font,48);

	// Títulos y resultados
	for(int i=0;i<nrandomizer;i++){
		textFont(font,48);
		size = fontsize_title;
		textSize(size);
		x = dx*(i%3);
		y = y0 + dy*(i/3);

		t = ran[i].getTitle(); // Title
		tw = textWidth(t);
		text(t,x + (dx-tw)/2, y);
		y += size;

		textFont(fontres,48);

		for(String s : ran[i].getList()){
			if(i!= 3 && i!=4){ // Si no son bailarines ni músicos
			size = fontsize_results;
			}
			else{
			size = 34;
			}

			if(i != 5){ // Si no es duración
				do{
					textSize(size);
					t = s;
					tw = textWidth(t);
					size--;
				}
				while(tw>dx);
				text(t,x + (dx-tw)/2, y);
				y += size;

			}
			else{ // Duración
				if(estado!=TIMERIDLE && estado!=TIMERRUN && estado!=TIMERFINISHED){
				timer.setFromSecondsString(s);
				}
				t = timer.formattedTimeMinSec();
				textSize(size);
				tw = textWidth(t);
				text(t,x + (dx-tw)/2, y);

			}

		}


	}

	// Draw timer
	if(estado==TIMERIDLE || estado==TIMERRUN || estado==TIMERFINISHED || estado==T1 || estado == T2){
		textFont(fonttime,80);
		if(timer.remainingSec()<=10){ // Queda poco tiempo
			if(estado!=T2){ // Si está acabando o en finished, deja su alpha igual
				fill(255,0,0,200);
			}
			else{
				fill(255,0,0,200*(1.0-alphaTxt)); // En transición, fade out
			}
		}
		else{
			if(estado==T1 || estado==T2){
				fill(255,200*(1.0-alphaTxt));
			}
			else{
				fill(255,200);
			}
		}
		textSize(80);
		/*
		if(estado==TIMERRUN){
		println(timer.timerMillis);
		}
		*/
		t = timer.formattedTime();
		tw = textWidth(t);
		text(t,(width-tw)/2,80);
		text(t,(width-tw)/2,height-40);
	}
}


void drawBackground(){
	if(estado == TIMERFINISHED && (millis()/500)%2==0){
		background(200);

		if(flashTransition){
			timerRuleta--;
			flashTransition = false;
		}
	}
	else{
		background(20);
		flashTransition = true;
	}

	if(estado!=TIMERIDLE && estado!=TIMERRUN && estado!=TIMERFINISHED){ // Letras random

		textFont(font,48);
		c = "";
		int r = 0;
	
		size = 20;
		int x,y;
		int sepy = size;
		int sepx = size;
		textSize(size);

		switch(estado){
			case T1:
				alphaTxt = map(timerRuleta,0,timerTInicial,0,1);
			break;
			case T2:
				alphaTxt = map(timerTInicial-timerRuleta,0,timerTInicial,0,1);
			break;
			default:
				alphaTxt = 1.0;
			break;
		}
		for(y=0;y<height+sepy;y+=sepy){
			for(x=0;x<width+sepx;x+=sepx){
				if(random(1)<0.5){
				r = int(random(text.length()));
				}
				else{
				r = (r+1)%text.length();
				}
				c = ""+text.charAt(r);
				textSize(size*random(1,1.8));
	
				if(estado==RUN && random(1)<0.2){
				fill(255,random(70,90)*alphaTxt);
				}
				else{
				fill(255,random(10,40)*alphaTxt);
				}
				text(c,x,y);
	
			}
		}

	}

}

void resetTimerTotal(){
	timertotal.setFromSeconds(60*TIMERTOTALTIME);
	escenaFinal =false;
}

boolean isTimeForEscenaFinal(){
	return timertotal.remainingSec()<REMAININGTIMETOEND*60;
}
