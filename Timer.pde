/*
    Ruleta
    On-stage software for Ruleta: Dance, music and interactive digital media improvisation session.

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Timer{
	int targetTime;

	int startMillis;
	int elapsedMillis;
	int targetMillis;
	int timerMillis;

	String formattedTime;

	boolean isRunning;

	Timer(){
		isRunning = false;
	}

	public void setFromSecondsString(String s){
		targetTime = int(s);
		targetMillis = targetTime*1000;
		timerMillis = targetMillis;
	}

	public void setFromSeconds(int s){
		targetTime = s;
		targetMillis = targetTime*1000;
		timerMillis = targetMillis;
	}

	public void start(){
		isRunning = true;
		startMillis = millis();
	}

	public void update(){
		if(isRunning){
			elapsedMillis = millis() - startMillis;
			timerMillis = targetMillis - elapsedMillis;
			if(elapsedMillis > targetMillis){
				isRunning = false;
				timerMillis = 0;
			}
		//	println(timerMillis);

		}

	}

	public boolean isRunning(){
		return isRunning;
	}

	public String formattedTime(){
		String s;
		int min, sec, dec;

		min = timerMillis/60000;
		sec = (timerMillis%60000)/1000;
		dec = ((timerMillis%60000)%1000)/10;

		return String.format("%02d:%02d.%02d",min,sec,dec);
	}

	public String formattedTimeMinSec(){
		String s;
		int min, sec;

		min = timerMillis/60000;
		sec = (timerMillis%60000)/1000;

		return String.format("%02d:%02d",min,sec);
	}

	public int remainingSec(){
		return timerMillis/1000;
	}

	public float elapsedPercentage(){
		return (elapsedMillis*100.0/targetMillis);
	}

	public float remainingPercentage(){
		return (timerMillis*100.0/targetMillis);
	}



}
